import './StartButton.css';

const StartButton = ({ startGame }) => {
    return (
        <button
            onClick={startGame}
            className='start-button'
        >
            START
        </button>
    )
}

export default StartButton