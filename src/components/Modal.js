import React from 'react';
import './Modal.css';

const Modal = ({ totalTime, winningPercentage, closeModal, openModal }) => {

    /**
     * Checks if the openModal boolean is closed
     */
    if (!openModal) { return null; }

    return (
        <div className='backdrop' onClick={closeModal}>
            <div className='modal-container'>
                <button className='close-button' onClick={closeModal}>Close</button>
                <h1>You Win!!</h1>
                <div className='score-container'>
                    <div className='stat-container'>
                        <div className='stat-item'>
                            <h3>% of Correct Clicks</h3>
                            <p>{winningPercentage}%</p>
                        </div>
                        <div className='stat-item'>
                            <h3>Total Time</h3>
                            <p>{totalTime}s</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    )
}

export default Modal