import './Timer.css';

const Timer = ({ time }) => {
    return (
        <div className='timer'>
            <h3 className='timer-title'>Timer</h3>
            <h2>{time}</h2>
        </div>
    )
}

export default Timer