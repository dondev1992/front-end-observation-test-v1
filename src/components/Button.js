import { useState } from 'react'
import './Button.css';

const Button = ({
    button,
    side,
    gameStarted,
    updateNumberSequence,
    shuffleNumbers,
    sequencedNumbers,
    countClicks,
    addToWinningNumbers,
    winningNumbers,
    didWin,
    startAndStopTimer
}) => {
    const [buttonColor, buttonColorSet] = useState('white');

    /**
     * Checks if the button should be one color of another
     * @param {number} id 
     * @returns boolean
     */
    const changeColor = (id) => {
        for (const number of winningNumbers) {
            if (number === id) {
                return true;
            }
        }
        // console.log(winningNumbers)
    }
    
    /**
     * Checks to see if the clicked button is the right button
     * @param {number} buttonNumber 
     * @param {[]} arrayOfOddOrEvenNumbers 
     */
    const isCorrect = (buttonNumber, arrayOfOddOrEvenNumbers) => {
        let tempArray = [...sequencedNumbers];
        countClicks(buttonNumber);
        if (buttonNumber === tempArray[0]) {
            tempArray.splice(0, 1)
            updateNumberSequence(tempArray);
            addToWinningNumbers(buttonNumber);
            changeColor(buttonNumber);
            // buttonColorSet(true);
            if (tempArray.length === 0) {
                didWin();
                startAndStopTimer();
                console.log(tempArray);
            }
            console.log(buttonNumber + " Correct");
        } else {
            shuffleNumbers(arrayOfOddOrEvenNumbers);
            console.log(buttonNumber + " InCorrect");
        }
    }


    return (
        <button
            className='number-button'
            style={{ backgroundColor: changeColor(button.id) ? '#F35B04' : '#F7B801', color: changeColor(button.id) ? 'black' : "black" }}
            onClick={() => (gameStarted && isCorrect(button.id, side))}
            value={button.id}
            id={button.id}
        >
            {button.id}
        </button>
    )
}

export default Button