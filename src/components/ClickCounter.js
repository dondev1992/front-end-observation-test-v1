import React from 'react';
import './ClickCounter.css';

const ClickCounter = ({ count }) => {
    return (
        <div className='display-count'>
            <h4>{count}</h4>
        </div>
    )
}

export default ClickCounter