import './App.css';
import React, { useState, useEffect } from 'react';
import Button from './components/Button';
import Modal from './components/Modal';
import StartButton from './components/StartButton';
import ClickCounter from './components/ClickCounter';
import Timer from './components/Timer';

function App() {
  const [evenButtons, evenButtonsSet] = useState([]);
  const [oddButtons, oddButtonsSet] = useState([]);
  const [sequencedNumbers, sequencedNumbersSet] = useState([]);
  const [countA, countASet] = useState(0);
  const [countB, countBSet] = useState(0);
  const [winningNumbers, winningNumbersSet] = useState([]);
  const [winningPercentage, winningPercentageSet] = useState(0);
  const [winMessage, winMessageSet] = useState(false);
  const [time, timeSet] = useState(0);
  const [isRunning, isRunningSet] = useState(false);
  const [openModal, openModalSet] = useState(false);
  const [gameStarted, gameStartedSet] = useState(false);

  let allButtons = [];
  let oButtons = [];
  let evButtons = [];
  
  /**
   * Starts the genearation of buttons only when the browser has been refreshed
   */
  useEffect(() => generateNumberSequence, []);


  /**
   * Creates and runs the timer function
   */
  useEffect(() => {
    let intervalId;
    if (isRunning) {
      intervalId = setInterval(() => timeSet(time + 1), 1000);
    }
    return () => clearInterval(intervalId);
  }, [isRunning, time])


  /**
   * Generates the number sequence, creates the buttons, sorts the buttons, and shuffles the buttons
   */
  const generateNumberSequence = () => {
    let button = { "id": 0 }

    let number = 0;
    let aNumbers = [];

    while (number < 25) {
      number++;
      button = {
        "id": number,
        "color": "white"
      };
      // console.log(button)
      aNumbers.push(number);
      allButtons.push(button);
      if (button.id % 2 === 0) {
        evButtons.push(button);
      } else {
        oButtons.push(button);
      }
    }
    console.log(allButtons)
    // console.log(evenButtons)
    shuffleNumbers(evButtons)
    shuffleNumbers(oButtons)
    evenButtonsSet([...evButtons]);
    oddButtonsSet([...oButtons]);
    sequencedNumbersSet([...aNumbers]);
  }


  /**
   * Function to start or restart the game. Resets all buttons, timers, and counters
   */
  const startGame = () => {
    console.log("Starting game")
    evenButtonsSet([]);
    oddButtonsSet([]);
    sequencedNumbersSet([]);
    countASet(0);
    countBSet(0);
    winningNumbersSet([]);
    winningPercentageSet(0);
    winMessageSet(false);
    isRunningSet(true);
    resetTimer(0);
    gameStartedSet(true)
    generateNumberSequence();
  }

  const startAndStopTimer = () => {
    isRunningSet(!isRunning);
  };

  const resetTimer = () => {
    timeSet(0);
  }

  const getWinningPercentage = () => {
    winningPercentageSet((25 / (countA + countB) * 100).toFixed(2));
  }


  /**
   * Adds the winning buttons' id to an array of winning numbers for keeping track of the game
   * @param {number} winningNumber 
   * @returns updated winningNumbers array
   */
  const addToWinningNumbers = (winningNumber) => {
    return winningNumbersSet([...winningNumbers, winningNumber])
  }

  const updateNumberSequence = (array) => {
    return sequencedNumbersSet(array);
  }

  const countClicks = (buttonNumber) => {
    if (buttonNumber % 2 === 0) {
      countBSet(prevState => prevState + 1)
    } else {
      countASet(prevState => prevState + 1)
    }
  }

  const shuffleNumbers = (array) => {
    return array.sort(() => Math.random() - 0.5);
  }

  function didWin() {
    winMessageSet(true)
    // console.log("WIN")
    openModalSet(true);
    return getWinningPercentage();
  }

  return (
    <div className="App">
      <div className='title'>
        <h1>Observation Test</h1>
      </div>

      <Modal
        winningPercentage={winningPercentage}
        totalTime={time}
        closeModal={() => openModalSet(false)}
        openModal={openModal}
      />
      <div className='container'>
        <div className='divider'>
          <div className='grid-container' id='left-container'>
            {oddButtons && oddButtons.map((button, index) => {
              return (
                <Button
                  gameStarted={gameStarted}
                  button={button}
                  addToWinningNumbers={addToWinningNumbers}
                  shuffleNumbers={shuffleNumbers}
                  updateNumberSequence={updateNumberSequence}
                  countClicks={countClicks}
                  sequencedNumbers={sequencedNumbers}
                  side={oddButtons}
                  key={index}
                  winningNumbers={winningNumbers}
                  didWin={didWin}
                  startAndStopTimer={startAndStopTimer}
                />
              )
            })}
          </div>
          <h3>Click Count</h3>
          <ClickCounter count={countA} />
        </div>
        <div className='center-display'>
          <StartButton startGame={() => startGame()} />
          <Timer time={time} />
        </div>
        <div className='divider'>
          <div className='grid-container'>
            {evenButtons && evenButtons.map((button, index,) => {
              return (
                <Button
                  gameStarted={gameStarted}
                  button={button}
                  addToWinningNumbers={addToWinningNumbers}
                  shuffleNumbers={shuffleNumbers}
                  updateNumberSequence={updateNumberSequence}
                  countClicks={countClicks}
                  sequencedNumbers={sequencedNumbers}
                  side={evenButtons}
                  key={index}
                  winningNumbers={winningNumbers}
                  didWin={didWin}
                  startAndStopTimer={startAndStopTimer}
                />
              )
            })}
          </div>
          <h3>Click Count</h3>
          <ClickCounter count={countB} />
        </div>
      </div>
    </div>
  );
}

export default App;
